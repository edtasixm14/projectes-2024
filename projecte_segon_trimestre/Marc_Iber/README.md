# 🏫  UOC 2023/2024-1

- [Aula català](#📂-fitxers-per-la-pràctica)
- [Aula castellano](#📂-archivos-para-la-práctica)

```bash
|- node-app
|  |- app
|- reverse-proxy
|- docker-compose.yml
|- data-mongo.js
```

## 📂 Fitxers per la pràctica

En aquest projecte trobareu l'estructura i fitxers necessaris per començar amb la pràctica.

El directori 📂 `app`conté el fitxers del servidor web `node + express`. Aquest directori 🖐 NO s'ha de modificar.

👉 Cal afegir el fitxers necessaris a  📁 `reverse-proxy` per configurar el reverse proxy i modificar els diferents `Dockerfile` corresponents i el fitxer `docker-compose`en funció de cada un dels exercicis.


## 📂 Archivos para la práctica

En este proyecto encontraréis la estructura y archivos necesarios para empezar con la práctica.

El directorio 📂 `app` contiene los archivos del servidor web `node + express`. Este directorio 🖐 NO debe modificarse.

👉 Hay que añadir los archivos necesarios a 📁 `reverse-proxy` para configurar el reverse proxy y modificar los diferentes `Dockerfile` correspondientes y el archivo `docker-compose` en función de cada uno de los ejercicios.
<br />

# INDEX

- **[Introducció](#Introducció)**

- **[Preparació de l'entorn](#Preparació de l'entorn)**

    - **[Instal·lació MV amb RAID](#Instal·lació MV amb RAID)**

- **[MongoDB](#MongoDB)**
    - **[Què és?](#Què és?)**
    - **[Característiques](#característiques)**
    - **[Configuració de MongoDB](#configuració)**

- **[NodeJS](#NodeJS)**
    - **[Què és?](#Què és?)**
    - **[Característiques](#característiques)**
    - **[Configuració de NodeJS](#configuració)**

- **[Reverse-proxy](#reverse-proxy)**
    - **[Què és?](#Què és?)**
    - **[Característiques](#característiques)**
    - **[Configuració de Reverse-proxy](#configuració)**

- **[Tallafoc](#Tallafoc)**
    - **[Què és?](#Què és?)**
        - **[IPTABLES](#iptables)**
    - **[Característiques](#característiques)**
    - **[Configuració de Tallafoc](#configuració)**

- **[Nota important](#important)**

<br />
<br />

## INTRODUCCIÓ<a name="Introducció"></a>

En aquest 2n projecte dut a terme durant el mes de febrer i març, hem pogut aprendre i consolidar diversos coneixements els quals ens serviran per al futur quan se'ns presentin circumstàncies o problemes semblants, ja sigui laboralment o per compte pròpia.

Aquest projecte ha tingut com a objectiu saber com fer una implementació d'un servidor de base de dades, d'un servidor web, d'un servidor reverse-proxy i d'un tallafoc i, fer les configuracions pertinents per poder aconseguir que tots els serveis estiguessin sincronitzats entre si. 

<i>Exemple visual de la infraestructura que hem de construir.</i><br />
![alt text](images-doc/image2.png)<br />
<b>*</b>

A continuació, us presentarem i explicarem pas a pas com hem aconseguit fer aquestes implementacions i configuracions seguint els passos del fitxer PDF proporcionat per la UOC.
<br />
<br />
## Preparació de l'entorn<a name="Preparació de l'entorn"></a>
En aquest apartat el que farem serà crear el nostre entorn de treball per poder fer adequadament aquest projecte. <br />
Per fer-ho utilitzarem la nostra pròpia màquina (host) des d'on farem comprovacions i una màquina virtual (client) on estarà tota la infraestructura dels serveis.
<br>
#### <i>Instal·lació MV amb RAID</i><a name="Instal·lació MV amb RAID"></a> 

El que farem serà crear la màquina virtual que farà de client des de virtual box.

##### Passos per la creació de la MV:

1. Crear la màquina virtual amb la imatge Debian 12, amb un espai de disc de 10 GB i amb els altres components hardware que ens doni per defecte virtual box.

2. El següent serà afegir un nou disc per crear el RAID.
    - Per fer-ho hem hagut d'anar a <i>Settings</i> -> <i>Storage</i> -> Controler:Sata.
    - Un cop dins clicar a l'opció de <i>Create</i>.
    - Escollir que el disc sigui VDI (Virtualbox Disk Image).
    - Acceptar que tingui el mateix espai que el de la nostra MV.

3. Un cop afegit aquest segon disc, entrem dins de la MV per començar la instal·lació.

##### Passos per la creació del RAID en la nostra MV:

Durant la instal·lació de la màquina virtual arribarem a l'apartat  <b>Partition disk</b> on podrem crear el RAID que necessitem per aquest projecte, en aquest cas el RAID1, ja que el que voldrem és redundància. 
Per aconseguir-ho farem el següent:
- Anirem a <i>Configure software RAID</i>. 
- Direm de crear un <i>MD (multiple device) device</i>.
- Escollim com a tipus de RAID el RAID1.
- Direm que aquesta RAID inclourà els dos discs durs que hem creat anteriorment dins de la MV.

##### Xarxa de la nostra MV:

Per poder fer comprovacions del que fem en la MV amb el nostre ordinador (host), posarem que la xarxa estigui en la mateixa que la del <i>host</i>. Per fer-ho anirem a l'apartat de la MV <i>Network</i> i indicarem que estigui en mode <b>Bridge Adapter</b>. Amb això fet, tant la MV com el nostre ordinador estaran en la mateixa xarxa.

<i>Mostra de la configuració de la xarxa</i><br />
![alt text](images-doc/image6.png)

##### Comprovació de la configuració feta:
Ara toca fer les comprovacions de les configuracions fetes en els punts anteriors, per això mirarem com estan creats els <i>sistemes de fitxers</i>, si el RAID1 s'ha creat correctament i si hi ha connexió entre la MV i el nostre ordinador.
Per saber a quins dispositius ens referim quan posem les IPs a l'hora de fer proves de connexions: host (10.200.243.212) i client (10.200.243.154).
- <i>Comprovació sistema de fitxers</i><br />
    ![alt text](images-doc/image.png)

- <i>Comprovació creació del RAID1</i><br />
    ![alt text](images-doc/image3.png)

- <i>Comprovació connexions entre dispositius</i>
    - client -> host<br />
    ![alt text](images-doc/image4.png)

    - host -> client<br />
    ![alt text](images-doc/image5.png)

Amb aquestes comprovacions fetes, ja podem donar com a finalitzat la part de creació de la màquina virtual.
<br />
<br />

## MongoDB<a name="MongoDB"></a>
Per poder posar-nos en context sobre que estem fent amb MongoDb en aquest projecte, hem volgut separar aquest apartat en tres punts:
- Què és MongoDB
- Característiques
- Configuració de MongoDB referent al nostre projecte
<br />

#### <i>Què és?</i><a name="Què és?"></a>
MongoDB va començar a desenvolupar-se l'any 2007 per la companyia que en aquell moment es deia 10gen (actualment  MongoDB Inc.) mentre estaven desenvolupant al mateix temps una <i>plataforma com a servei</i> (PAAS).
Cap a l'any 2009 MongoDB va passar a ser un producte independent de codi obert i es van començar a oferir suport comercial i altres serveis.
En el 2011 es va publicar la versió 1.4, la qual es va considerar com a una base de dades llesta per poder posar-se en producció. 

MongoDB és un sistema de gestió de base de dades NoSQL, orientada a documents, pensada per a ser ràpida, escalable i fàcil d'utilitzar.
Aquesta utilitza documents flexibles en comptes de taules i files per processar i emmagatzemar diverses formes de dades.  
<br />

#### <i>Característiques</i><a name="característiques"></a>
Les característiques més rellevants de la base de dades MongoDB són les següents: Consultes Ad hoc, Indexació, Anàlisi de rendiment de queris i Balanç de càrrega.  
- Consultes Ad hoc -> Suporta consultes de rangs, expressions regulars i cerques per camps.
- Indexació -> Qualsevol camp pot ser indexat i, també és possible fer índexs secundaris.
- Anàlisi de rendiment de queris -> MongoDB proporciona una eina que permet determinar el rendiment de les nostres consultes i conèixer possibles defectes en alguna estructura. 
- Balanç de càrrega -> Té la capacitat d'executar-se en diversos servidors, balancejant la càrrega i/o duplicant les dades per poder mantenir el sistema funcionant en cas que hi hagi algun problema de hardware.

MongoDB també suporta alguns dels llenguatges  més utilitzats avui en dia, els quals són els següents: NodeJS, Python, JS (Java Script), Perl, PHP, Ruby, Java, C i C++.

Encara que MongoDB té alguns desavantatges per la poca maduresa que té en comparacions amb altres productes, com poden ser els problemes de compatibilitat, té molts avantatges les quals ho contraresten com són la seva velocitat de consultes, el seu senzill escalament i que sigui de codi obert.  
<br />

#### <i>Configuració de MongoDB</i><a name="configuració"></a>
Ara que ja ens hem posat en context, toca parlar de la configuració que hem fet en aquesta part del projecte.

L'objectiu en aquest apartat del projecte ha sigut crear un container que contingui el servei MongoDB amb les configuracions pertinents perquè les dades no es perdin, per tenir el seu port propagat al ordinador client i, crear una xarxa i posar variables d'entorn que ens serviran per a fer connexió amb la base de dades de MongoDB.

Configuracions a fer:

El que ens demanen és engegar el servei MongoDB amb el seu port (27017) exposat a l'ordinador client i amb un volum per poder tenir persistència de dades, és a dir. que les dades no s'esborrin al reiniciar el servei, crear una xarxa anomenada <i>db-network</i> i posar variables d'entorn vessant-nos en el fitxer <b>data-mongo.js</b>. Tot això estarà ficat dins de l'arxiu <i>docker-compose.yml</i>.  

1. Configuració
    - <i>Configuració del fitxer docker-compose.yml</i><br />
    ![alt text](images-doc/image7.png)

2. Desplegament
    - <i>Comprovació d'execució exitosa</i><br />
    ![alt text](images-doc/image8.png)

    - <i>Comprovació que el port MongoDB està escoltant i és accessible</i><br />
    ![alt text](images-doc/image9.png)

    - <i>Comprovar si el port està propagat a l'ordinador client (MV)</i><br />
    ![alt text](images-doc/image10.png)

3. Accés a la base de dades

Per poder accedir-hi hem posat la següent ordre: <b>>mongosh --host 10.200.243.154 -u admin -p --authenticationDatabase admin</b>

<i>Demostració gràfica accedint a la base de dades</i><br />
![alt text](images-doc/image11.png)
<br />
<br />

## NodeJS<a name="NodeJS"></a>
<br />

#### <i>Què és?</i><a name="Què és?"></a>
Node.js va ser creada en 2009 per Ryan Dahl amb la intenció de proporcionar als desenvolupadors la capacitat d'utilitzar JavaScript per a seqüències d'ordres del costat del servidor i unificar el desenvolupament d'aplicacions web al voltant d'un únic llenguatge de programació.

Al gener de 2010, es va introduir un administrador de paquets per a Node.js per facilitar als programadors publicar i compartir el codi font dels paquets de Node.js i simplificar la instal·lació, desinstal·lació i actualitzacions.

El 2011, Microsoft i Joyent es van unir per desenvolupar una versió nativa de Node.js per a Windows, ampliant la quantitat de sistemes operatius que podia admetre i brindant als desenvolupadors més opcions que mai.


<br />

#### <i>Característiques</i><a name="característiques"></a>
Les característiques més rellevants de NodeJS són les següents: Processament I/O alhora, Real time, Múltiples connexions, Funcions Map/Redueix, Balanceig de càrrega.

Processament I/O alhora -> NodeJS permet manejar operacions d'entrada/sortida (I/O) de manera no bloquejant i asíncrona. És a dir, que pot gestionar múltiples sol·licituds d'entrada/sortida simultàniament sense bloquejar el que està en execució. Això és especialment útil en aplicacions web i servidors on cal manejar moltes sol·licituds concurrents.

Real time -> Té la capacitat per manejar connexions bidireccionals de forma eficient, pot facilitar la comunicació en temps real entre el client i el servidor mitjançant tecnologies com ara WebSockets o protocols similars.
Això permet una interacció instantània entre usuaris i actualitzacions en temps reals a la interfície d'usuari sense necessitat de recarregar la pàgina.

Múltiples connexions -> Pot manejar eficientment moltes connexions alhora, fins a milers de connexions. Això ens serà molt útil per a servidors web que necessitin atendre a múltiples clients simultàniament.

Funcions Map/Redueix -> Permet transformar i analitzar grans conjunts de dades de manera eficient.

Balanç de càrrega -> Té la capacitat d'executar-se en diversos servidors, balancejant la càrrega i/o duplicant les dades per poder mantenir el sistema funcionant en cas que hi hagi algun problema de hardware

Avantatges i desavantatges

Avantatges
- Fàcil d'aprendre.
- Llibertat en el desenvolupament d'aplicacions.
- Comunitat activa: APIs, Codi a Github,...
- I/O sense bloqueig -> Rapidesa.

Desavantatges
- API inestable (noves versions sense compatibilitat amb versions anteriors). Requereix fer canvis a les aplicacions.
- Més temps de desenvolupament.
- No és apropiat per a apps que necessiten molta computació.
- Ecosistema (rpm,...) encara immadur.
<br />

#### <i>Configuració de NodeJS</i><a name="configuració"></a>
Ara que ja ens hem posat en context, toca parlar de la configuració que hem fet en aquesta part del projecte.

L'objectiu en aquest apartat del projecte ha sigut crear un container que contingui el servei web nodeJS, el qual es construirà a partir del fitxer Dockerfile dins del directori <i>node-app</i> i que tingui les configuracions pertinents perquè fagi connexió amb la base de dades de MognoDB.

Configuracions a fer:

El que ens demanen és engegar el servei web nodeJS amb el seu port (3000) exposat a l'ordinador client amb el port (5000), crear una xarxa anomenada <i>web-network</i> i assignar la xarxa db-network, on estarà també el servei MongoDB i, posar variables d'entorn vessant-nos en el fitxer <i>server.js</i> dins del directori node-app/app. Tot això estarà ficat dins de l'arxiu <i>docker-compose</i>.

1. Configuració
    - <i>Configuració del fitxer docker-compose.yml</i><br />
    ![alt text](images-doc/image12.png)

    - <i>Configuració del fitxer Dockerfile</i><br />
    ![alt text](images-doc/image13.png)

2. Desplegament
    - <i>Comprovació d'execució exitosa</i><br />
    ![alt text](images-doc/image14.png)

    - <i>Comprovació de l’estat dels ports exposats</i><br />
    ![alt text](images-doc/image15.png)

3. Mostra de la web<br />
    ![alt text](images-doc/image16.png)
<br />
<br />

## Reverse-proxy<a name="reverse-proxy"></a>
<br />

#### <i>Què és?</i><a name="Què és?"></a>

Un servidor reverse-proxy és un tipus de servidor proxy que normalment es troba darrere d'un firewall (tallafoc) en una xarxa privada. Aquest servidor té com a objectiu dirigir les sol·licituds dels clients al servidor backend apropiat i proporciona un nivell addicional d'abstracció i control per a garantir el flux fluid del tràfic de xarxa entre client i servidor. 

En aquesta pràctica com a servidor de reverse-proxy utilitzarem Nginx.
##### <i>Nginx</i><a name="nginx"></a>

Nginx és un servidor web/proxy invers lleuger d'alt rendiment i un proxy per a protocols de correu electrònic el qual utilitza un enfocament asincrònic basat en esdeveniments per manejar sol·licituds. En comptes de crear un nou procés per a cada sol·licitud feta pel client, maneja múltiples sol·licituds en un sol process de treball.
Gràcies al fet que està basat en esdeveniments, pot processar amb èxit milions de sol·licituds simultànies, té molt bona escalabilitat i proporciona un rendiment web optimitzat.

És de software lliure i de codi obert, llicenciat sota la Llicencia BSD simplificada. D'aquest també existeix una versió comercial anomenada Nginx Plus.
<br />

#### <i>Característiques</i><a name="característiques"></a>

Les característiques, en aquest cas del reverse-proxy Nginx, són les següents: 
- Pot processar gran quantitat de peticions amb menys recursos cadascuna, el que fa que les peticions siguin més lleugeres.
- Té un tamany livià.
- Té la capacitat de escalar facilment en hardwares poc potents.
- Proporciona contingut estàtic ràpidament.
- Té el seu sistema pròpi de móduls sòlids.
- Pot enviar solicituds dinàmiques a altres softwares segons sigui necessari.
<br />

#### <i>Configuració de Reverse-proxy</i><a name="configuració"></a>
Ara que ja ens hem posat en context, toca parlar de la configuració que hem fet en aquesta part del projecte.
<br />
<br />

## Tallafoc<a name="tallafoc"></a>
<br />

#### <i>Què és?</i><a name="Què és?"></a>
El tallafocs és un mur que es construeix en un punt entre l'ordinador o la xarxa interna que es vol protegir i una xarxa pública (és a dir, l'internet).
Examina cada dada que intenta transitar de, i cap a la xarxa interna, permetent el seu pas si compleix els criteris ja establerts i evitant-ne l'ingrés si no els compleix. 

És una capa de protecció i de seguretat que tant bloqueja l’accés no permès des d’una xarxa externa com restringir les connexions de la xarxa interna a l’exterior.

En aquesta pràctica com a tallafoc utilitzarem iptables.
<br />

##### <i>IPTABLES</i><a name="iptables"></a>
Iptables és un mòdul del nucli de Linux que s'encarrega de filtrar els paquets de xarxa, és a dir, és la part que s'encarrega de determinar quins paquets de dades volem que arribin fins al servidor i quins no. 

Igual que passa amb altres sistemes de tallafocs, iptables funciona a través de regles. És a dir, l'usuari mitjançant instruccions senzilles indica al tallafocs el tipus de paquets que ha de permetre entrar, els ports per on es poden rebre aquests paquets, el protocol utilitzat per a l'enviament de dades i qualsevol altra informació relacionada amb l'intercanvi de dades entre xarxes.

Quan en el sistema es rep o s'envia un paquet, es recorren en ordre les diferents regles fins a trobar una que compleixi les condicions. Un cop localitzada, aquesta regla s'activa fent sobre el paquet l'acció indicada. Gràcies a la seva robustesa, iptables s'ha convertit ara com ara en una de les eines més utilitzades per al filtratge de trànsit en sistemes Linux.
<br />

#### <i>Característiques</i><a name="característiques"></a>
En iptables existeixen tres grans grups de regles o taules:
1. Regles de filtrat -> Dins aquesta taula s'indiquen les regles de filtratge, és a dir, indiquem quins paquets seran acceptats, quins rebutjats o quins són omesos. 
Les cadenes que formen part d'aquesta taula són les següents:
    - INPUT: Fa referència als paquets que arriben al sistema.
    - OUTPUT: Filtrat dels paquets que surten de la nostra xarxa.
    - FORWARD: Referència al trànsit que el router reenvia a altres equips.

2. Taula NAT -> Mitjançant aquesta taula, s'indiquen les regles per realitzar l'emmascarament de l'adreça IP del paquet, redirigir ports o canviar l'adreça IP d'origen i destinació dels paquets. 
Dins aquesta taula ens podem trobar les següents cadenes predefinides:
    - PREROUTING: Mitjançant aquesta cadena, indiquem que es faci una determinada acció sobre el paquet abans que sigui enrutat. Per exemple, perquè des de l'exterior hi hagi accés a un servidor.
    - POSTROUTING: Permet realitzar una determinada acció abans que el paquet surti del tallafoc.
    - OUTPUT: Permet modificar els paquets generats al propi tallafocs abans de ser enrutats.

3. Taula Mangle -> És la taula que recull les diferents regles que actuen sobre els flags (opcions) dels paquets. Tots els paquets passen per aquesta taula.

L'estructura general d'una regla és la següent.
<b>Iptables –t [taula] operació cadena paràmetres acció</b>
<br />

#### <i>Configuració de Tallafoc</i><a name="configuració"></a>
Ara que ja ens hem posat en context, toca parlar de la configuració que hem fet en aquesta part del projecte.
<br />
<br />

## Nota important<a name="important"></a>

- Tota imatge que tingui el simbol "<b>*</b>" sota d'una imatge són imatges propietat de la UOC.

- La infraestructura on em fet tota la configuració per aquest projecte ha sigut proporcionada per la UOC amb el següent enllaç de github:
 <i><b>git clone https://github.com/jestebangr/prac20231-orig.git</b></i>