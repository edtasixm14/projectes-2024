# Neteja de regles i cadenes existents
iptables -F
iptables -X

# Establir política predeterminada a DROP para INPUT, FORWARD y OUTPUT
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT ACCEPT

# Permetre connexions entrants per els ports 80 i 443
iptables -A FORWARD -p tcp --dport 80 -j ACCEPT
iptables -A FORWARD -p tcp --dport 443 -j ACCEPT

