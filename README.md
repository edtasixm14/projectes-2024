# Projectes 2024

## Escola del Treball de Barcelona

### ASIX Administració de sistemes informàtics en xarxa

#### Curs 2023-2024

#### M14 Projecte

ASIX M14 Projectes Escola del treball de Barcelona. Curs 2023-2024.


**Projecte primer trimestre**

  * Descripció: [Projecte_primer_trimestre.pdf](https://gitlab.com/edtasixm14/projectes-2023/-/blob/main/M14-Projecte-primer-trimestre.pdf)
  
  * [Pau + Diego](https://gitlab.com/pausanchez02/api-project.git)
  * [Jordi + Aleix](https://gitlab.com/jordijsmx/projecte/-/tree/main?ref_type=heads)
  * [Brian + Antonio + Iber]()
  * [Martí + Tomàs + Marc]()
  * [Javier + Oscar]()

**Projecte segon trimestre**

  * Descripció: [Projecte_segon_trimestre.pdf](https://gitlab.com/edtasixm14/projectes-2024/-/blob/main/projecte_segon_trimestre/M14_projectes_segon_trimestre_23-24.pdf)

  * [Pau + Diego + Oscar](https://gitlab.com/pausanchez02/proyecte-segon-trimestre)
  * [Jordi + Aleix](https://gitlab.com/jordijsmx/projecte/-/tree/main/segon?ref_type=heads)
  * [Brian + Antonio](https://gitlab.com/PROJECTESM14/projecte-2)
  * [Marc + Íber](https://gitlab.com/projecte-m14/projecte2)


**Projecte tercer trimestre**

  * Descripció:




