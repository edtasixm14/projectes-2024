# PROYECTE API/OAUTH

### API

~~~
Una api és una interficie de programació d'aplicacions o un conjunt de funcions i procediments, ofert desde una biblioteca informàtica per 
ser utilitzat conjuntament per un altre programa, el qual interacciona amb aquest en qüestió.

Una aplicació web dinàmica necessita tenir implementar un programari de connexió per tal de poder-se comunicar amb el servidor, d'on 
recull les dades que mostra als seus usuaris en pantalla. Aquest programari de connexió és el que es coneix com a API.

En el nostre cas, utilitzarem l'API de Spotify per poder accedir als recursos/base de dades d'aquesta, els quals es troben allotjats en 
forma d'objectes JSON en una sèrie de rutes o Endpoints definits al servidor, accessibles via HTTP mitjançant tecnologia AJAX.

Pel funcionament de les API s'utilitza una arquitectura client - servidor on un servidor es defineix com a passiu o esclau el qual espera
peticions, quan rep una la processa i retorna una resposta que sol ser un HTML o altres tipus de documents com XML o JSON.
~~~

### API EN REST

~~~
EL terme REST va ser establert a partir d'una tesi de Roy Thomas Fielding de l'any 2000 sobre com aprofitar el protocol HTTP per tal de
desenvolupar aplicacions distribuïdes a través de xarxa. Perquè un servidor web es pugui anomenar REST ha de satisfer 6 restriccions, 
l'ultima d'aquestes opcionals:

1) Interfície Uniforme: la part fonamental del servei REST.

    - Identificació dels recursos a través de les URI o URL.
    - Manipulació dels recursos a través de les seves representacions.
    - Missatges autodescriptius que contenen tota la informació necessària.
    - HATEOAS. El client interactua amb el servidor per complet mitjançant hipermèdia proporcionada dinàmicament per aquest segon.

2) Client-Servidor:
    - Separació clara entre el client i el servidor per millorar la portabilitat del codi al no emmagatzemar les dades el client.
    - Desenvolupament independent del client i del servidor, amb una interfície uniforme.

3) Stateless:
    - Absència de manteniment d'estat entre les peticions consecutives, amb tota la informació necessària en cada missatge del client.

4) Cacheable:
    - Respostes del servidor poden ser emmagatzemades en memòria cau per millorar l'escalabilitat i el rendiment, reduint 
    les interaccions amb el servidor.

5) Layered System:
    - Permet l'existència de capes intermediàries entre el client i el servidor, com servidors en cau o altres elements millorant la 
    seguretat.

6) Code on Demand (opcional):
    - El servidor pot proporcionar temporalment codi executant a la màquina del client per ampliar la funcionalitat.

Beneficis per a l’USUARI/ÀRIA: un servei REST facilita enormement la càrrega de l’aplicació web al dispositiu mòbil, ja que aquesta 
consumeix dades de l’API externa i, per tant, no és necessari emmagatzemar cap dada a la memòria interna.
    
Beneficis pel PROGRAMADOR/A: poder disposar de fonts de dades de tot tipus a les quals poder accedir lliurement i permetre a l’USUARI
o USUÀRIA interactuar amb aquestes dades i obtenir-ne nous resultats, és, sens dubte, un gran avantatge.
~~~

### AJAX

~~~
Les aplicacions web dinàmiques utilitzen tecnologia AJAX per tal de fer les peticions i processar les respostes provinents del 
servidor REST. 

AJAX ens permet:

    - Llegir dades d’un servidor web (una vegada la pàgina s’ha carregat).
    - Actualitzar una pàgina web sense haver de recarregar la pàgina.
    - Enviar dades a un servidor web.

Tot i que les sigles d'AJAX són d’Asynchronous JavaScript And XML, les aplicacions AJAX poden utilitzar no només XML per transportar 
les dades, sinó que moltes vegades aquest transport es fa via JSON treballant amb la resposta d’una manera molt lleugera.

El fet que AJAX sigui asíncron vol dir que les dades que són demanades es carreguen en segon pla, sense interferir en cap cas en la 
presentació i el comportament de la pàgina. És a dir, que l’aplicació carrega, en primer lloc, els arxius HTML, CSS i JavaScript al 
frontend de l’usuari o usuària, i posteriorment executa les peticions de dades via AJAX. Una vegada s’obtenen els resultats de la 
consulta, la pàgina s’actualitza amb les noves dades sense necessitat d’haver-se de recarregar de nou. Normalment, el que s’actualitza 
són parts d’una pàgina i no la pàgina completa.

Una possible tècnica per programar en AJAX és mitjançant el framework jQuery, que assisteix el treball de qui programa en dos nivells: 
en el costat client, ofereix funcions JavaScript per enviar peticions al servidor via HTTP, i, en el costat servidor, processa les 
peticions, busca la informació i transmet la resposta de tornada al client.
~~~

## REST - HTTP

~~~
El protocol web de transferencia de dades HTTP va ser dissenyat per permetre la comunicació entre servidors i clients.

És important que coneguem quins són els mètodes principals que ens ofereix HTTP per tal de poder treballar correctament amb aquest 
protocol dins les nostres peticions AJAX:

1) Mètode GET: s'utilitza per demanar dades d'un recurs epecífic, com ara una URI o ENDPOINT.

2) Mètode POST: s'utilitza per recollir dades d'un servidor com el GET, pero en aquest cas ens permet enviar dades dins la mateixa 
petició sense que puguin ser interceptades, per tal de crear o actualitzar un recurs del servidor. 

3) Mètode PUT: s'utilitza per enviar dades a un servidor per tal de crear o actualitzar un recurs, permetent repetir una mateixa 
petició POST a diferentes part del nostre codi.

4) Mètode DELETE: s'utilitza per esborrar un recurs determinat
~~~

~~~
Dins de tots els objectes de JavaScript, l'element més important d'AJAX és el XMLHttpRequest. Aquest objecte ens porta moltes 
comoditats a l'hora de treballar:

- Actualitzar una pàgina web sense haver de recarregar la pàgina sencera
- Fer una petició de dades, rebre dades i enviar dades a un servidor una vegada la pàgina s'ha carregat.
~~~


## JQUERY 

~~~
jQuery és una llibreria de JavaScript àmpliament utilitzada per a simplificar i millorar el desenvolupament frontend en entorns 
web. Amb una sintaxi senzilla i consistent, jQuery facilita la manipulació eficient del DOM, permetent als desenvolupadors 
seleccionar, modificar i animar elements HTML amb facilitat.

Dissenyada per abordar les complexitats i les diferències entre els navegadors, jQuery ofereix una interfície homogènia per a 
les interaccions amb el DOM, eliminant molts dels problemes de compatibilitat que poden sorgir en el desenvolupament web.

A més de les seves funcionalitats bàsiques, jQuery ofereix suport per a animacions, gestió d'esdeveniments, peticions AJAX i altres
 tasques comunes. La seva modularitat i extensibilitat han contribuït a la seva pervivència, tot i l'aparició de noves tecnologies.

Encara avui en dia, jQuery pot ser una elecció viable per als desenvolupadors que busquen una solució fiable i àgil per al 
desenvolupament frontend, tot i que és important tenir en compte altres alternatives i tendències del sector.
~~~


## STATUS HTTP

~~~
 XMLHttpRequest()
    onreadystatechange
    readyState
    status

Vegem els valors principals que pot prendre la propietat readyState:
• readyState = 0 => petició no inicialitzada
• readyState = 1 => connexió de servidor establerta
• readyState = 2 => petició rebuda
• readyState = 3 => petició en procés
• readyState = 4 => petició finalitzada i resposta llesta

Ara veurem els valors més comuns que acostuma a prendre la propietat status en el cas que
la petició AJAX es processi satisfactòriament:
• Status 200 => Resposta estàndard quan la petició està OK.
• Status 201 => En cas de peticions per POST, petició completada i nou recurs creat.

Per la seva banda, els errors 400 fan referència als errors de la part client:
• Status 400 = Bad Request => La petició no es pot completar per un error de sintaxi.
• Status 401 = Unauthorized => La petició és legal però el servidor la rebutja.

Acostuma a saltar en els casos que l’autorització és necessària però errònia.
• Status 403 = Forbidden => La petició és legal però el servidor la rebutja.
• Status 404 = Not found => L’URL indicat no es troba.

I els errors 500 indiquen errors de servidor:
• Status 500 = Internal Server Error => Missatge d’error genèric.
• Status 503 = Service Unavailable => El servidor no està disponible
~~~

## OAUTH

~~~

OAuth (Open Authorization) és un protocol d'autorització que permet a usuaris donar accés als seus recursos sense revelar les seves 
credencials. Es basa en l'ús de tokens, com els "access tokens", i utilitza un flux d'autorització per a autenticació segura en 
aplicacions web i mòbils.

En el context d'un usuari que vol iniciar sessió en una aplicació amb l'autenticació d'OAuth de Google, el procés és el següent:

- L'usuari sol·licita iniciar sessió i és redirigit a la pàgina d'autenticació de Google.

- Després d'autenticar-se amb les seves credencials de Google, l'usuari dóna permís per accedir a determinades dades o funcionalitats.

- Google emet un "access token" a l'aplicació que ha sol·licitat l'accés.

- Aquest "access token" s'utilitza per autenticar l'usuari i accedir als recursos autoritzats sense exposar les credencials de Google.

- L'aplicació pot utilitzar aquest "access token" per fer crides a l'API de Google en nom de l'usuari per obtenir o enviar dades.


Així, OAuth proporciona un marc segur per a l'autenticació i autorització, permetent als usuaris controlar quina informació comparteixen 
amb les aplicacions terceres sense haver de revelar les seves contrasenyes.
~~~













