# Projecte1 2024-2025

## 2ASIX-M14

Llistat dels projectes fets a M14

Per clonar el repositori:
```bash
git clone https://gitlab.com/m14-projectes/projecte1.git
```

Per pujar els apunts:
```bash
git add . ; git commit -m "projecte" ; git push
```
