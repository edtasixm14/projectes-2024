# Exemple d'Autorització de Spotify amb Codi i PKCE

Aquesta aplicació mostra la informació del teu perfil de Spotify utilitzant el [Codi d'Autorització amb PKCE](https://developer.spotify.com/documentation/web-api/tutorials/code-pkce-flow)
per concedir permisos a l'aplicació.

## Utilitzant les teves pròpies credencials

Hauràs de registrar la teva aplicació i obtenir les teves pròpies credencials des del [Tauler de Desenvolupadors de Spotify](https://developer.spotify.com/dashboard).

- Crea una nova aplicació al tauler i afegeix `http://localhost:8080` a la llista d'URL de redirecció de l'aplicació.
- Un cop hagis creat la teva aplicació, actualitza el `client_id` i `redirect_uri` al fitxer `public/app.js` amb els valors obtinguts de la configuració de l'aplicació al tauler.

## Execució de l'exemple

Des d'una consola:

    $ npm start

Després, obre `http://localhost:8080` en un navegador.

