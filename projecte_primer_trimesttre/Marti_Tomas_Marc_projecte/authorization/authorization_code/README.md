# Exemple d'Autorització de Spotify amb Codi

Aquesta aplicació mostra la informació del teu perfil de Spotify utilitzant el [Codi d'Autorització](https://developer.spotify.com/documentation/web-api/tutorials/code-flow)
per concedir permisos a l'aplicació.

## Instal·lació

Aquest exemple s'executa amb Node.js. Al [seu lloc web](http://www.nodejs.org/download/) pots trobar instruccions sobre com instal·lar-ho.

Instal·la les dependències de l'aplicació amb la següent comanda:

    $ npm install

## Utilitzant les teves pròpies credencials

Hauràs de registrar la teva aplicació i obtenir les teves pròpies credencials des del [Tauler de Desenvolupadors de Spotify](https://developer.spotify.com/dashboard).

- Crea una nova aplicació al tauler i afegeix `http://localhost:8888/callback` a la llista d'URL de redirecció de l'aplicació.
- Un cop hagis creat la teva aplicació, actualitza el `client_id`, `redirect_uri` i `client_secret` al fitxer `app.js` amb les credencials obtingudes de la configuració de l'aplicació al tauler.

## Execució de l'exemple

Des d'una consola:

    $ npm start

Després, obre `http://localhost:8888` en un navegador.

