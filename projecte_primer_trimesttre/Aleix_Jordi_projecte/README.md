# Projecte ASIX 2024

- <https://gitlab.com/jordijsmx/projecte> 

## Integrants del Grup

- Aleix Ridameya
- Jordi Jubete

## Primer Trimestre

## Web API REST amb autenticació OAUTH

### **Què és una API?**

API és l'acrònim de interfície de programació d'aplicacions o _Application Programming Interface_. És un conjunt de protocols que s'utilitzen per especificar formalment la comunicació entre dos components de software. 

Es basa en el model **client-servidor** on el client és el que sol·licita obtenir els recursos o realitzar alguna operació sobre aquestes dades, mentre que el servidor és el que entrega o processa aquestes dades a sol·licitud del client.

Hi ha diferent tipus de APIs, les més utilitzades són les REST o _Representational State Transfer_ APIs.

### **Què és REST?**

Per a que una API sigui REST ha de complir els següents criteris:
- **Arquitectura client-servidor** composta d'un client i un servidor, recursos i gestió de sol·licituds HTTP.
- **Stateless** o sense estat, implica que la informació del client no s'emmagatzema entre les sol·licituds. Cada una és independent i esta desconnectada de la resta.
- **Dades emmagatzemades a caché** i que optimitzin les interaccions client-servidor.
- **Interfície Uniforme** entre els elements, per a que la informació es transmeti de forma estandaritzada.
- **Sistema de capes** que organitza en jerarquíes els servidors que participen en la recuperació de la informació sol·licitada pel client.
- **Codi disponible** segons es soliciti de manera que, quan un client ho requereixi, rebi codi executable per ampliar les seves funcions. 

Si una API compleix tots aquests criteris, es considera **RESTful**, si nó, és **RESTless.**

### **Mètodes HTTP**

Un recurs és qualsevol informació que pot ser anomenada, ja sigui un document, imatge, col·lecció de recursos o més. 

REST utilitza quatre mètodes HTTP per a gestionar les sol·licituds que s'envien al servidor:

- **GET:** Permet al servidor trobar les dades sol·licitades i les envia al client.
- **PUT:** El servidor actualitzarà una entrada a la base de dades.
- **POST:** Permet al servidor crear una nova entrada a la base de dades.
- **DELETE:** Permet al servidor esborrar una entrada a la base de dades.

### **Codis d'estat HTTP**

Els codis d'estat HTTP son codis de tres dígits que són retornats per un servidor com a resposta d'una sol·licitud HTTP d'un client. Indiquen el resultat i informació sobre una operació realitzada. Els codis d'estat estan dividits en cinc categories:

1. **Informació (1xx):** S'utilitza per transmetre unformació a nivell de protocol de transferència.
2. **Codis d'èxit (2xx):** Indica que la sol·licitat del client ha estat acceptada i processada amb èxit pel servidor. El codi més comú és 200 OK, com a sol·licitud exitosa.
3. **Redireccions (3xx):** Indica que el client ha de realitzar alguna acció adicional per a completar la sol·licitud. S'utilitzen per redirigir al client a una altra ubicació.
4. **Error de client (4xx):** La sol·licitud del client no s'ha pogut realitzar o conté errors. Són errors de la banda client, com sol·licituds mal formades o accés no autoritzat.
5. **Error de servidor (5xx):** El servidor no ha pogut realitzar una sol·licitud vàlida del client. Aquests codis assenyalen problemes en el servidor, com errors interns o sobrecàrrega temporal.

### **AJAX**

AJAX és l'acrónim de _Asynchronous Javascript and XML_. És un conjunt de tècniques de desenvolupament web que permeten a les aplicacions web treballar de forma **asíncrona**, processant qualsevol sol·licitud al servidor en **segon pla.**

És una combinació de quatre tecnologies:
- **HTML/XHTML** per al llenguatge principal i **CSS** per a la presentació.
- **JSON** per a l'intercanvi i el transport de dades.
- L'objecte **XMLHttpRequest** per a la comunicació asíncrona.
- **Javascript** per a gestionar el contingut de la pàgina, interactuar amb l'usuari dinàmicament i unir totes aquestes tecnologies.

Tant **Javascript** com **JSON** treballen de manera asíncrona, per tant, qualsevol aplicació que utilitzi AJAX pot enviar i rebre dades sense necessitat de **recarregar** la pàgina.

### **OAuth**

És l'acrònim de _Open Authorization_, és un estàndard dissenyat per permetre que una pàgina web o aplicació accedeixin a recursos allotjats per altres aplicacions web en nom de l'usuari. És un protocol **d'autorització** i no d'autenticació, per tant, està dissenyat principalment com un medi per concedir accés a un conjunt de recursos, com les API.

Utilitza **tokens d'accés**, que són dades que representen l'autorització per accedir als recursos en nom de l'usuari.

**refresh token**

El fluxe d'un intercanvi en una sol·licitud seria el següent:
1. El client sol·licita l'autorització proporcionant el **client id i el client secret** com identificació i un URI a on enviar el token d'accés.
2. El servidor autentica al client.
3. El propietari del recurs interactúa amb el servidor per concedir l'accés.
4. El servidor dóna un codi d'autorització o **token d'accés.**
5. Amb el token, el client sol·licita accés al recurs del servidor.

## **Webgrafía**

https://blog.hubspot.es/website/que-es-api-rest

https://blog.postman.com/rest-api-examples/

https://auth0.com/es/intro-to-iam/what-is-oauth-2#tipos-de-concesi%C3%B3n-en-oauth-20

https://www.hostinger.es/tutoriales/que-es-ajax

https://restfulapi.net/http-status-codes/

</div>