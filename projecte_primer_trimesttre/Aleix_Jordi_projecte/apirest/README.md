# 1.1 Mostrar els id de /posts en consola
Per poder mostrar el /posts hem d'anar a la web "https://jsonplaceholder.typicode.com/" i accedir a l'enllaç /posts.
Per poder-lo mostrar en consola hem de crear un fitxer .html amb aquest codi:
```
<!DOCTYPE html>
<html>
    <body>
        <h1>Connexio API REST utilitzant el metode XMLHttpRequest()</h1>
        <br/>
        <div id="elements"></div>
    </body>
    <script>
        var xmlhttp = new XMLHttpRequest();
        var url = "https://jsonplaceholder.typicode.com/posts";
        xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            console.log(myArr);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
    </script>
</html>
```
El codi és d'una pàgina web que amb un script fa una petició i mostra el resultat per la consola del navegador web

Mostra:

![hola](images/exercici1_1.PNG)

# 1.2 Mostrar els id de /posts
Per mostrar els id de /posts utlitzem aquest codi.

```
<!DOCTYPE html>
<html>
    <body>
        <h1>Connexio API REST utilitzant el metode XMLHttpRequest()</h1>
        <br/>
        <div id="elements"></div>
    </body>
    <script>
    var xmlhttp = new XMLHttpRequest();
    var url = "https://jsonplaceholder.typicode.com/posts";
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            console.log(myArr);
            myFunction(myArr);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
    console.log(xmlhttp);
    function myFunction(array) {
        var sortida = "";
        for(var i = 0; i < array.length; i++) {
        sortida += array[i].id + " - " + array[i].title + "<br/>";
        console.log(array[0].id + " - " + array[0].title);
        }
        document.getElementById("elements").innerHTML = sortida;
    }
    </script>
</html>
```
El codi és d'una pàgina web que amb un script, fa una petició i la mostra des de la pròpia pàgina web. 


Mostra:

![hola](images/exercici1_2.PNG)

# 1.3 Mostrar id+email dels comments del post 1
Aquest exercici tracte de mostrar els id, els emails i els comments aquests els agafara d'aquesta web "https://jsonplaceholder.typicode.com/comments".

Cada script realitzarà una funció:
- Mostra per la consola-web /post/1/comments.
- Mostra només els camps id i email per la propia pàgina web.
- Mostra el id i email però només del primer dels comentaris per la consola-web.
Codi:
```
<!DOCTYPE html>
<html>
    <body>
        <h1>Connexio API REST utilitzant el metode XMLHttpRequest()</h1>
        <br/>
        <div id="elements"></div>
    </body>
    <script>
    var xmlhttp = new XMLHttpRequest();
    var url = "https://jsonplaceholder.typicode.com/posts/1/comments";
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            console.log(myArr);
        }
    };
    /*xmlhttp.open("GET", url, true);
    xmlhttp.send();
    console.log(xmlhttp);/*
    </script>
    <script>
    var xmlhttp = new XMLHttpRequest();
    var url = "https://jsonplaceholder.typicode.com/posts/1/comments";
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            console.log(myArr);
            myFunction(myArr);
        }
        };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
    console.log(xmlhttp);
        function myFunction(array) {
            var sortida = "";
            for(var i = 0; i < array.length; i++) {
                sortida += array[i].id + " - " + array[i].email + "<br/>";
                console.log(array[0].id + " - " + array[0].email);
            }
        document.getElementById("elements").innerHTML = sortida;
        }
    </script>
    <script>
        var xmlhttp = new XMLHttpRequest();
        var url = "https://jsonplaceholder.typicode.com/posts/1/comments";
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var myArr = JSON.parse(this.responseText);
                console.log(myArr);
            console.log(myArr[0].id + " - " + myArr[0].email);
        }
        };
        /*xmlhttp.open("GET", url, true);
        xmlhttp.send();
        console.log(xmlhttp);*/
    </script>
</html>
```
Mostra:

![hola](images/exercici1_3.PNG)



